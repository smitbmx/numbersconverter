# README #

### Prerequisites
* [.NET Core 3.0](https://dotnet.microsoft.com/download/dotnet-core/3.0)
* [NodeJS](https://nodejs.org/en/) _(Angular requirenment)_
* Visual Studio 2019 or VS 2019 For Mac

### Run
Open the solution file in Visual Studio 2019 or Visual Studio For Mac and just press F5. Only the first time compilation takes time because of the restoring NPM packages for the Angular application. 

### Solution (contains 8 projects):
##### 1) Akqa.NumbersConverter 
 The startup project, which contains REST API Service and Angular application.
#####  2) ConverterService.Abstraction 
 Contains **IConverterService** interface for Converters.
##### 3) CustomConverter 
Custom implementation.
#####  4) HumanizerConverter 
Implementation which use 3rd patry package [Humanizer](https://github.com/Humanizr/Humanizer). 
##### 5) CustomConverter.Tests.Unit
Unit tests for custom implementation of the converter service.
##### 6) HumanizerConverter.Tests.Unit
Unit tests for humanizer implementation of the converter service.
##### 7) Akqa.NumbersConverter.Tests.Unit 
Contains unit tests for Akqa.NumbersConverter.
##### 8) Akqa.NumbersConverter.Tests.Functional
Contains functional tests for **ConverterController** with each service implementation. 

#### Service Configuration
The application support change services in runtime without any project recompilation.
How to:
1) Open _appsettings.json_ file
2) Find **CurrentConverter** node and change **Name** value to one of the services: **ConverterService** or **HumanizerConverterService**. Currently the both implementation have identical output.

#### Custom service
You are able to create your own implementation of the service. To make it, implement the **IConverterService** interface and register in the Startup class:
```
services.AddScoped<IConverterService, CustomConverter.ConverterService>();
```