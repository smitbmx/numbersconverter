using System;
using Xunit;

namespace CustomConverter.Tests.Unit
{
    public class CustomConverterTests
    {
        [Theory]
        [InlineData(0, "zero dollars")]
        [InlineData(1, "one dollar")]
        [InlineData(2, "two dollars")]
        [InlineData(3, "three dollars")]
        [InlineData(5, "five dollars")]
        [InlineData(10, "ten dollars")]
        [InlineData(-10, "minus ten dollars")]
        public void Convertation_Dollars_Tests(decimal number, string expectedResult)
        {
            //Arrange
            ConverterService converterService = new ConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(0.01, "zero dollars and one cent")]
        [InlineData(0.02, "zero dollars and two cents")]
        [InlineData(0.03, "zero dollars and three cents")]
        [InlineData(0.05, "zero dollars and five cents")]
        [InlineData(0.10, "zero dollars and ten cents")]
        public void Convertation_ZeroDollarsWithCents_Tests(decimal number, string expectedResult)
        {
            //Arrange
            ConverterService converterService = new ConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(1.01, "one dollar and one cent")]
        [InlineData(1.02, "one dollar and two cents")]
        [InlineData(1.03, "one dollar and three cents")]
        [InlineData(1.05, "one dollar and five cents")]
        [InlineData(1.10, "one dollar and ten cents")]
        public void Convertation_OneDollarsWithCents_Tests(decimal number, string expectedResult)
        {
            //Arrange
            ConverterService converterService = new ConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(2.01, "two dollars and one cent")]
        [InlineData(3.02, "three dollars and two cents")]
        [InlineData(4.03, "four dollars and three cents")]
        [InlineData(5.05, "five dollars and five cents")]
        [InlineData(10.10, "ten dollars and ten cents")]
        public void Convertation_FewDollarsWithCents_Tests(decimal number, string expectedResult)
        {
            //Arrange
            ConverterService converterService = new ConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(12.01, "twelve dollars and one cent")]
        [InlineData(23.02, "twenty-three dollars and two cents")]
        [InlineData(34.03, "thirty-four dollars and three cents")]
        [InlineData(45.05, "forty-five dollars and five cents")]
        [InlineData(70.10, "seventy dollars and ten cents")]
        public void Convertation_DozensDollarsWithCents_Tests(decimal number, string expectedResult)
        {
            //Arrange
            ConverterService converterService = new ConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(101.01, "one hundred and one dollars and one cent")]
        [InlineData(230.02, "two hundred and thirty dollars and two cents")]
        [InlineData(340.03, "three hundred and forty dollars and three cents")]
        [InlineData(450.05, "four hundred and fifty dollars and five cents")]
        [InlineData(700.10, "seven hundred  dollars and ten cents")]
        public void Convertation_HundredsDollarsWithCents_Tests(decimal number, string expectedResult)
        {
            //Arrange
            ConverterService converterService = new ConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(1000001, "one million and one dollars")]
        [InlineData(10000002, "ten million and two dollars")]
        [InlineData(2000000000, "two billion  dollars")]
        [InlineData(2147483647, "two billion one hundred and forty-seven million four hundred and eighty-three thousand six hundred and forty-seven dollars")]
        [InlineData(2147483646.99, "two billion one hundred and forty-seven million four hundred and eighty-three thousand six hundred and forty-six dollars and ninety-nine cents")]
        public void Convertation_HugeEdgeNumbers_Tests(decimal number, string expectedResult)
        {
            //Arrange
            ConverterService converterService = new ConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }
    }
}
