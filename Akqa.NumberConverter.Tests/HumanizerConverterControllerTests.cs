using System;
using Akqa.NumbersConverter.Controllers;
using Akqa.NumbersConverter.Models;
using ConverterService.Abstraction;
using Xunit;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System.Collections.Generic;
using HumanizerConverter;
using Akqa.NumbersConverter.Configuration;
using Microsoft.Extensions.Options;

namespace Akqa.NumbersConverter.Tests.Functional
{
    public class HumanizerConverterControllerTests
    {
        ConverterController controller;
        ILogger<ConverterController> logger;
        IOptions<CurrentConverter> currentConverterOptions;
        string currentControllerName = "HumanizerConverterService";
        HumanizerConverterService humanizerConverterService;

        public HumanizerConverterControllerTests()
        {
            List<IConverterService> services = new List<IConverterService>();
            var currentConverter = new CurrentConverter { Name = currentControllerName };

            humanizerConverterService = new HumanizerConverterService();
            currentConverterOptions = Options.Create(currentConverter);
            services.Add(humanizerConverterService);
            logger = Substitute.For<ILogger<ConverterController>>(); 
            controller = new ConverterController(services, logger, currentConverterOptions);
        }

        [Fact]
        public void Convert_NullInputDataModel_Test()
        {
            //Arrange
            InputDataModel inputDataModel = null;

            //Act
            TestHelper.SimulateValidation(inputDataModel, controller);
            var response = controller.Convert(inputDataModel);

            //Assert
            Assert.NotNull(response);
            Assert.False(response.IsSuccess);
            Assert.Null(response.Name);
            Assert.Null(response.ConvertedNumber);
        }

        [Theory]
        [InlineData(-1, "MINUS ONE DOLLAR", "Ivan Brygar")]
        [InlineData(0, "ZERO DOLLARS", "Ivan Brygar")]
        [InlineData(999.99, "NINE HUNDRED AND NINETY-NINE DOLLARS AND NINETY-NINE CENTS", "Ivan Brygar")]
        [InlineData(2147483647, "TWO BILLION ONE HUNDRED AND FORTY-SEVEN MILLION FOUR HUNDRED AND EIGHTY-THREE THOUSAND SIX HUNDRED AND FORTY-SEVEN DOLLARS", "Ivan Brygar")]
        public void Convert_EdgeNumbers_Test(decimal number, string expectedResult, string inputName)
        {
            //Arrange
            InputDataModel inputDataModel = new InputDataModel
            {
                Name = inputName,
                Number = number
            };

            //Act
            TestHelper.SimulateValidation(inputDataModel, controller);
            var response = controller.Convert(inputDataModel);

            //Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccess);
            Assert.Equal(inputName, response.Name);
            Assert.Equal(expectedResult, response.ConvertedNumber);
        }

        [Theory]
        [InlineData(null, "ZERO DOLLARS", "Ivan Brygar")]
        public void Convert_PotentialErrors_Test(decimal number, string expectedResult, string inputName)
        {
            //Arrange
            InputDataModel inputDataModel = new InputDataModel
            {
                Name = inputName,
                Number = number
            };

            //Act
            TestHelper.SimulateValidation(inputDataModel, controller);
            var response = controller.Convert(inputDataModel);

            //Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccess);
            Assert.Equal(inputName, response.Name);
            Assert.Equal(expectedResult, response.ConvertedNumber);
        }

        [Theory]
        [InlineData(1, "Ivan BrygarIvan BrygarIvan BrygarIvan BrygarIvan BrygarIvan Brygar")]
        [InlineData(null, null)]
        [InlineData(-1001, "Ivan Brygar")]
        [InlineData(1, "I")]
        public void Convert_NoValidModel_Test(decimal number, string inputName)
        {
            //Arrange
            InputDataModel inputDataModel = new InputDataModel
            {
                Name = inputName,
                Number = number
            };

            //Act
            TestHelper.SimulateValidation(inputDataModel, controller);
            var response = controller.Convert(inputDataModel);

            //Assert
            Assert.NotNull(response);
            Assert.False(response.IsSuccess);
        }
    }
}
