﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Akqa.NumbersConverter.Tests.Functional
{
    public static class TestHelper
    {
        // Mimic the behaviour of the model binder which is responsible for Validating the Model
        public static void SimulateValidation(object model, ControllerBase controller)
        {
            if (model == null)
            {
                AddModelError("nullReference", "The Model is null", controller);
                return;
            }

            var validationContext = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(model, validationContext, validationResults, true);
            foreach (var validationResult in validationResults)
            {
                AddModelError(validationResult.MemberNames.First(), validationResult.ErrorMessage, controller);
            }
        }

        private static void AddModelError(string key, string message, ControllerBase controller)
        {
            controller.ModelState.AddModelError(key, message);
        }
    }
}
