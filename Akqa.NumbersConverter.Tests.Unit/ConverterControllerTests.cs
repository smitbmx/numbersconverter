using System;
using Xunit;
using NSubstitute;
using Akqa.NumbersConverter.Controllers;
using Microsoft.Extensions.Logging;
using ConverterService.Abstraction;
using Akqa.NumbersConverter.Models;
using System.Collections.Generic;
using Akqa.NumbersConverter.Configuration;
using Microsoft.Extensions.Options;

namespace Akqa.NumbersConverter.Tests.Unit
{
    public class ConverterControllerTests
    {
        private IConverterService _converterService;
        private ILogger<ConverterController> _logger;
        List<IConverterService> _services;
        IOptions<CurrentConverter> currentConverterOptions;
        ConverterController controller;

        public ConverterControllerTests()
        {
            _converterService = Substitute.For<IConverterService>();
            _converterService.CurrentName.Returns("mock");
            _services = new List<IConverterService>();
            _services.Add(_converterService); 
            _logger = Substitute.For<ILogger<ConverterController>>();
            currentConverterOptions = Options.Create(new CurrentConverter { Name = "mock" });
            controller = new ConverterController(_services, _logger, currentConverterOptions);
        }

        [Fact]
        public void ConverterController_NullDataModel_Test()
        {
            //Arrange
            InputDataModel inputDataModel = null;

            //Act
            var response = controller.Convert(inputDataModel);

            //Assert
            Assert.NotNull(response);
            Assert.False(response.IsSuccess);
            Assert.Null(response.Name);
            Assert.Null(response.ConvertedNumber);
        }

        [Theory]
        [InlineData("Ivan Brygar", "Ivan Brygar", 10, "TEN DOLLARS")]
        public void ConverterController_DataModel_Test(string name, string expectedName, decimal number, string expectedConvertedNumber)
        {
            //Arrange
            InputDataModel inputDataModel = new InputDataModel
            {
                Name = name,
                Number = number
            };

            _converterService.ConvertToWords(number).Returns(expectedConvertedNumber);

            //Act
            var response = controller.Convert(inputDataModel);

            //Assert
            Assert.NotNull(response);
            Assert.Equal(expectedName, response.Name);
            Assert.Equal(expectedConvertedNumber, response.ConvertedNumber);
        }

        [Fact]
        public void ConverterController_SimulateException_Test()
        {
            //Arrange
            InputDataModel inputDataModel = new InputDataModel
            {
                Name = "A",
                Number = int.MaxValue
            };

            _converterService.ConvertToWords(int.MinValue).ReturnsForAnyArgs(x => { throw new Exception(); });

            //Act
            var response = controller.Convert(inputDataModel);

            //Assert
            Assert.NotNull(response);
            Assert.Throws<Exception>(() => _converterService.ConvertToWords(int.MinValue));
        }
    }
}
