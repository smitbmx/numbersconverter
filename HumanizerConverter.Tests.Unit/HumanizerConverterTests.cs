using System;
using Xunit;

namespace HumanizerConverter.Tests.Unit
{
    public class HumanizerConverterTests
    {
        [Theory]
        [InlineData(0, "zero dollars")]
        [InlineData(0.0, "zero dollars")]
        [InlineData(0.00, "zero dollars")]
        [InlineData(1, "one dollar")]
        [InlineData(2, "two dollars")]
        [InlineData(3, "three dollars")]
        [InlineData(5, "five dollars")]
        [InlineData(10, "ten dollars")]
        [InlineData(-10, "minus ten dollars")]
        [InlineData(0.01, "zero dollars and one cent")]
        [InlineData(0.02, "zero dollars and two cents")]
        [InlineData(0.03, "zero dollars and three cents")]
        [InlineData(0.05, "zero dollars and five cents")]
        [InlineData(0.10, "zero dollars and ten cents")]
        [InlineData(1000001, "one million and one dollars")]
        [InlineData(10000002, "ten million and two dollars")]
        [InlineData(2000000000, "two billion dollars")]
        [InlineData(int.MinValue, "minus two billion one hundred and forty-seven million four hundred and eighty-three thousand six hundred and forty-eight dollars")]
        [InlineData(int.MaxValue, "two billion one hundred and forty-seven million four hundred and eighty-three thousand six hundred and forty-seven dollars")]
        [InlineData(2147483647, "two billion one hundred and forty-seven million four hundred and eighty-three thousand six hundred and forty-seven dollars")]
        [InlineData(2147483646.99, "two billion one hundred and forty-seven million four hundred and eighty-three thousand six hundred and forty-six dollars and ninety-nine cents")]
        public void Convertation_Dollars_Tests(decimal number, string expectedResult)
        {
            //Arrange
            HumanizerConverterService converterService = new HumanizerConverterService();

            //Act
            string result = converterService.ConvertToWords(number).ToLower();

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(expectedResult, result);
        }
    }
}
