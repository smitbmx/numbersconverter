﻿using System;
namespace CustomConverter
{
    internal static class Constants
    {
        public const string Minus = "Minus";
        public const string Zero = "Zero";
        public const string ZeroDollars = "Zero Dollars";
        public const string Cent = "Cent";
        public const string Cents = "Cents";
        public const string Dollar = "Dollar";
        public const string Dollars = "Dollars";

        public static string[] UnitsMap = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        public static string[] TensMap = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
    }
}
