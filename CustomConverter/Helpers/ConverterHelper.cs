﻿using System;
namespace CustomConverter.Helpers
{
    public static class ConverterHelper
    {
        internal static string GetSingularOrPluralWord(int number, string singularWord, string pluralWord)
        {
            return number == 1 || number == -1 ? singularWord : pluralWord;
        }
    }
}
