﻿using System;
using ConverterService.Abstraction;
using CustomConverter.Helpers;

namespace CustomConverter
{
    public class ConverterService : IConverterService
    {
        public string CurrentName => nameof(ConverterService);

        public string ConvertToWords(decimal number)
        {
            if (number == 0)
                return Constants.ZeroDollars;

            if (number < 0)
                return string.Format("{0} {1}", Constants.Minus, ConvertToWords(Math.Abs(number)));

            string words = "";

            int intPortion = (int)number;
            decimal fraction = (number - intPortion) * 100;
            int decPortion = (int)fraction;

            words = string.Format("{0} {1}", NumberToWords(intPortion), ConverterHelper.GetSingularOrPluralWord(intPortion, Constants.Dollar, Constants.Dollars));
            if (decPortion > 0)
            {
                words += " and ";
                string coinWord = ConverterHelper.GetSingularOrPluralWord(decPortion, Constants.Cent, Constants.Cents);
                words += string.Format("{0} {1}", NumberToWords(decPortion), coinWord);
            }

            return words;
        }

        private static string NumberToWords(int number)
        {
            if (number == 0)
                return Constants.Zero;

            string words = "";

            if ((number / 1000000000) > 0)
            {
                words += NumberToWords(number / 1000000000) + " billion ";
                number %= 1000000000;
            }

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                if (number < 20)
                    words += Constants.UnitsMap[number];
                else
                {
                    words += Constants.TensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + Constants.UnitsMap[number % 10];
                }
            }

            return words;
        }
    }
}
