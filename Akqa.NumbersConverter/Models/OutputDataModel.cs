﻿using System;
namespace Akqa.NumbersConverter.Models
{
    public class OutputDataModel
    {
        public string Name { get; set; }
        public string ConvertedNumber { get; set; }
        public bool IsSuccess { get; set; }
    }
}
