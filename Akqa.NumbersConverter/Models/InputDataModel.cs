﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Akqa.NumbersConverter.Models
{
    public class InputDataModel
    {
        [Required]
        [MinLength(Constants.MinNameLength, ErrorMessage = Constants.TooShortNameLength)]
        [MaxLength(Constants.MaxNameLength, ErrorMessage = Constants.ExceededMaxLength)]
        public string Name { get; set; }

        [Required]
        [Range(Constants.MinAcceptableNumber, Constants.MaxAcceptableNumber, ErrorMessage = Constants.TheValueIsOutOfRange)]
        public decimal Number { get; set; }
    }
}
