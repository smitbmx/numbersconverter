﻿using System;
namespace Akqa.NumbersConverter
{
    public static class Constants
    {
        public const string ExceededMaxLength = "Exceeded max length characters.";
        public const string TooShortNameLength = "Too short name length.";
        public const string TheValueIsOutOfRange = "The value is out of range.";

        public const int MinNameLength = 2;
        public const int MaxNameLength = 50;

        public const int MinAcceptableNumber = -1000;
        public const int MaxAcceptableNumber = int.MaxValue;
    }
}
