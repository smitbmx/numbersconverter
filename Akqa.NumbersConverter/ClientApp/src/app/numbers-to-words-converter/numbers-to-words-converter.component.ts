import { Component, OnInit } from '@angular/core';
import { DataModel } from '../models/data.model';
import { ApplicationService } from '../services/application.service';
import { ResultDataModel } from '../models/result-data.model';

@Component({
  selector: 'app-numbers-to-words-converter',
  templateUrl: './numbers-to-words-converter.component.html',
  styleUrls: ['./numbers-to-words-converter.component.css']
})
export class NumbersToWordsConverterComponent implements OnInit {

  name: string;
  number: number;
  nameResult: string;
  numberResult: string;
  dataModel: DataModel = new DataModel();
  isSuccess: boolean = false;

  constructor(private converterService: ApplicationService) { }

  ngOnInit() {
  }

  convert() {
    this.dataModel.name = this.name;
    this.dataModel.number = this.number;

    this.converterService.convertNumber(this.dataModel).subscribe(result => {
      this.isSuccess = result.isSuccess;
      this.nameResult = result.name;
      this.numberResult = result.convertedNumber;
    });
  }
}
