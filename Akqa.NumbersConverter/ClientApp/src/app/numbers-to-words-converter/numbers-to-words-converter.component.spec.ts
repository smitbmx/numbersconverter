import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumbersToWordsConverterComponent } from './numbers-to-words-converter.component';

describe('NumbersToWordsConverterComponent', () => {
  let component: NumbersToWordsConverterComponent;
  let fixture: ComponentFixture<NumbersToWordsConverterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumbersToWordsConverterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumbersToWordsConverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
