import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { DataModel } from '../models/data.model';
import { ResultDataModel } from '../models/result-data.model';

@Injectable({
  providedIn: 'root'
})

export class ApplicationService {
  baseUrl: string;
  constructor(private httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  convertNumber(dataModel: DataModel): Observable<ResultDataModel> {
    var requstPath = this.baseUrl + 'Converter/Convert';
    return this.httpClient.post<ResultDataModel>(requstPath, dataModel);
  }
}
