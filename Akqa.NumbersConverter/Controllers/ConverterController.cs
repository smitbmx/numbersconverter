﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akqa.NumbersConverter.Configuration;
using Akqa.NumbersConverter.Models;
using ConverterService.Abstraction;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Akqa.NumbersConverter.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConverterController: ControllerBase
    {
        private readonly CurrentConverter _currentConverter;
        private readonly IConverterService _converterService;
        private readonly ILogger<ConverterController> _logger;

        public ConverterController(IEnumerable<IConverterService> converterServices, ILogger<ConverterController> logger, IOptions<CurrentConverter> currentConverter)
        {
            _currentConverter = currentConverter.Value;
            _converterService = converterServices.FirstOrDefault(s => s.CurrentName == _currentConverter.Name);
            _logger = logger;
        }

        [HttpPost("Convert")]
        public OutputDataModel Convert([FromBody]InputDataModel dataModel)
        {
            OutputDataModel outputDataModel = new OutputDataModel { IsSuccess = false };

            if (IsModelValid(dataModel))
            {
                try
                {
                    string convertedNumber = _converterService.ConvertToWords(dataModel.Number);

                    outputDataModel = GetOutputDataModel(dataModel.Name, convertedNumber.ToUpper());
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }

            return outputDataModel;
        }

        private bool IsModelValid(InputDataModel dataModel)
        {
            return ModelState.IsValid && dataModel != null;
        }

        private OutputDataModel GetOutputDataModel(string name, string convertedNumber)
        {
            OutputDataModel outputDataModel = new OutputDataModel
            {
                Name = name,
                ConvertedNumber = convertedNumber,
                IsSuccess = true
            };

            return outputDataModel;
        }
    }
}
