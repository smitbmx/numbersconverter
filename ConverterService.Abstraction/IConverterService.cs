﻿namespace ConverterService.Abstraction
{
    public interface IConverterService
    {
        string CurrentName { get; }

        string ConvertToWords(decimal value);
    }
}
