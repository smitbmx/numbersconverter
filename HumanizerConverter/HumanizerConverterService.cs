﻿using System;
using ConverterService.Abstraction;
using Humanizer;

namespace HumanizerConverter
{
    public class HumanizerConverterService : IConverterService
    {
        public string CurrentName => nameof(HumanizerConverterService);

        public string ConvertToWords(decimal number)
        {            
            int dollars = (int)number;
            decimal fraction = (number - dollars) * 100;
            int cents = (int)fraction;
            string centsPart = string.Empty;
            string dollarsPart = string.Format("{0} {1}", dollars.ToWords(), GetSingularOrPluralWord(dollars, Constants.Dollar, Constants.Dollars));

            if (cents != 0)
            {
                centsPart = string.Format(" and {0} {1}", cents.ToWords().Replace("minus", ""), GetSingularOrPluralWord(cents, Constants.Cent, Constants.Cents));
            }

            return $"{dollarsPart}{centsPart}";
        }

        private static string GetSingularOrPluralWord(int number, string singularWord, string pluralWord)
        {
            return number == 1 || number == -1 ? singularWord : pluralWord;
        }
    }
}
