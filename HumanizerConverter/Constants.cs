﻿using System;
namespace HumanizerConverter
{
    public static class Constants
    {
        public const string Cent = "Cent";
        public const string Cents = "Cents";
        public const string Dollar = "Dollar";
        public const string Dollars = "Dollars";
    }
}
